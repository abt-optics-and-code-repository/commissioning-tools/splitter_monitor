from PyQt5.QtWidgets import QApplication
from splitter_monitor import app_framework as af
import sys


def main(*args):
    app = QApplication(sys.argv)
    form = af.MyApp()
    form.show()

    return app.exec_()

if __name__ == '__main__':
    sys.exit(main(sys.argv))