"""
High-level tests for the  package.

"""

import splitter_monitor


def test_version():
    assert splitter_monitor.__version__ is not None
