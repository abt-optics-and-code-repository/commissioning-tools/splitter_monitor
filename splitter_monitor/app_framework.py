from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtCore import QThread, pyqtSignal
import matplotlib.pyplot as plt
import shapely as shp
from shapely.geometry import Point, Polygon, MultiPolygon, MultiPoint
from descartes.patch import PolygonPatch
from tsmoothie.smoother import LowessSmoother

import collections
from PyQt5 import uic
import os
import numpy as np
import pyjapc

from cern_general_devices.bsg import BSGSplitters
from splitter_monitor.mplwidget import MplWidget
import logging
from accwidgets.log_console import LogConsole, LogLevel, LogConsoleModel



class MyApp(QMainWindow):

    japc = pyjapc.PyJapc("SPS.USER.ALL", noSet=True)

    def __init__(self):
        super(MyApp, self).__init__()
        self.local_path = os.path.dirname(__file__)
        Form, _ = uic.loadUiType(
            self.local_path + "/window/mainwindow.ui"
        )
        self.ui = Form()
        self.ui.setupUi(self)

        self.ui.plotarea = MplWidget()
        self.ui.plotarea.setObjectName("plotArea")
        self.ui.PlotAreaLayout.addWidget(self.ui.plotarea)


        self.context = None
        self.main_widget = QWidget(self)
        self.showReference = False

        self.bsg = BSGSplitters(self.japc, timingSelectorOverride="")

        self.plot_keys = ["1v", "1h", "2v"]
        self.titles = ["Splitter 1 - V", "Splitter 1 - H", "Splitter 2 - V"]

        whole_area = Polygon([(-100, -100), (-100, 100), (100, 100), (100, -100)])

        material_upper_TCSC = self.get_material_upper(0.64, 8, 14, 36*np.pi/180, 60*np.pi/180, 100)
        material_upper_MSSB = self.get_material_upper(0, 8, 14, 36*np.pi/180, 60*np.pi/180, 100)
        material_lower = Polygon([(-100, -100), (-100, 0), (100, 0), (100, -100)])

        self.TCSC = whole_area.difference(material_upper_TCSC.union(material_lower))
        self.MSSB = whole_area.difference(material_upper_MSSB.union(material_lower))

        # self.visualizeData(self.bsg.getParameter(timingSelectorOverride="SPS.USER.SFTPRO1")[0])
        
        self.ui.Startmonitoring.clicked.connect(self.startMonitoring)

    def callback(self, param, new_value, header):

        if not header[0]["isFirstUpdate"]:
            data_all = self.bsg.callback_handler(param, new_value)
            self.visualizeData(data_all)
        else:
            logging.warning("First update...waiting for proper data")
        # try:
        #     data_all = self.bsg.callback_handler(param, new_value)
        # except:
        #     logging.error("error in handling the data...waiting for next one.")
        # try:
            # self.visualizeData(data_all)
        # except:
            # logging.error("Error in plotting data...")


    
    def startMonitoring(self):

        if self.ui.Startmonitoring.text() == "Start monitoring":
            
            self.japc.subscribeParam(self.bsg.all_vars, self.callback, getHeader=True)
            
            self.japc.startSubscriptions()
            
            self.ui.Startmonitoring.setText('Stop')


        elif self.ui.Startmonitoring.text() == "Stop":

            self.stopSubscription()

            self.ui.Startmonitoring.setText('Start monitoring')

    def _clear_plot(self):
        for ax in self.ui.plotarea.canvas.axs:
            ax.clear()


    def visualizeData(self, data):

        self._clear_plot()
        for ax, plot_key, title in zip(self.ui.plotarea.canvas.axs, self.plot_keys, self.titles):
            self.plot_splitter_grids_smooth(ax, data[plot_key], self.bsg.configs[plot_key], plane=plot_key[-1].upper(), label=plot_key)
            ax.set_title(title)

        self.ui.plotarea.canvas.fig.tight_layout()
        self.ui.plotarea.canvas.draw()

    def _norm(self, data):
        return (data - np.min(data)) / (np.max(data) - np.min(data)) * 20

    def _smooth(self, data, smooth_fraction=0.4, iterations=5):
        smoother = LowessSmoother(smooth_fraction=smooth_fraction, iterations=iterations)
        smoother.smooth(data)

        low, up = smoother.get_intervals('confidence_interval')
        
        return smoother.smooth_data[0], low[0], up[0]

    def plot_splitter_grids_smooth(self, ax, data, configs, plane="V", label=None):
    
        h_pos = []
        for ele in data.keys():
            if label is None:
                data_to_plot, low, up = self._smooth(data[ele])
            else:
                if "1" in label and "v" in label:
                    data_to_plot, low, up = self._smooth(data[ele])
                elif "1" in label and "h" in label:    
                    data_to_plot, low, up = self._smooth(data[ele], 0.3)
                elif "2" in label and "v" in label:    
                    data_to_plot, low, up = self._smooth(data[ele])
                data_to_plot = data_to_plot / max(data_to_plot) * 20
            data_to_plot = np.clip(data_to_plot, 0, np.max(data_to_plot))

            if plane == "V":
                x = np.linspace(0, len(data[ele]) * configs[ele]["PITCH"], len(data[ele])) + configs[ele]["OFFSET"]
                ax.add_patch(PolygonPatch(self.TCSC, facecolor='gray', edgecolor='red', zorder=2, alpha=0.2))
                p0 = ax.plot(data_to_plot * configs[ele]["DIRECTION"], x[::configs[ele]["SIGN"]], label=ele)
                ax.fill_betweenx(x[::configs[ele]["SIGN"]], 0, data_to_plot * configs[ele]["DIRECTION"], alpha=0.3)
                # ax.fill_betweenx(x[::configs[ele]["SIGN"]], up * configs[ele]["DIRECTION"], low * configs[ele]["DIRECTION"], alpha=0.3, color=p0[0].get_color())
                ax.set_ylim(-70, 70)
                ax.set_xlim(-40, 40)
            else:
                x = np.linspace(0, len(data[ele]) * configs[ele]["PITCH"], len(data[ele])) + configs[ele]["OFFSET"]
                ax.add_patch(PolygonPatch(self.TCSC, facecolor='gray', edgecolor='red', zorder=2, alpha=0.2))
                p0 = ax.plot(x[::configs[ele]["SIGN"]], data_to_plot, label=ele)
                ax.fill_between(x[::configs[ele]["SIGN"]], 0, data_to_plot, alpha=0.3)
                # ax.fill_between(x[::configs[ele]["SIGN"]], low[0], up[0], alpha=0.3, color=p0[0].get_color())
                mean_x = x[::configs[ele]["SIGN"]][np.argmax(data_to_plot == max(data_to_plot)).flatten()][0]
                h_pos.append(mean_x)
                ax.axvline(mean_x, ls='--')

                ax.set_ylim(-5, 30)
                ax.set_xlim(-10, 10)
       
        if plane == "H":
            ax.annotate(f"Delta x = {(h_pos[1] - h_pos[0]):.1f} mm", xy=(-7.5, 25), xytext=(-7.5, 25))
        ax.set_xlabel("x / mm")
        ax.set_ylabel("y / mm")
        ax.legend(fontsize=8, ncol=2, mode="expand", frameon=True)
        self.ui.plotarea.canvas.draw()

    def plot_splitter_grids(self, ax, data, configs, plane="V"):
    
        for ele in data.keys():
            mask = (np.abs(data[ele] - np.mean(data[ele])) < 3 * np.std(data[ele]))

            if plane == "V":
                data_to_plot = self._norm(data[ele][mask] * configs[ele]["DIRECTION"])
                x = np.linspace(0, len(data[ele]) * configs[ele]["PITCH"], len(data[ele])) + configs[ele]["OFFSET"]
                ax.add_patch(PolygonPatch(self.TCSC, facecolor='gray', edgecolor='red', zorder=2, alpha=0.2))
                p0 = ax.plot(data_to_plot, x[::configs[ele]["SIGN"]][mask], label=ele)
                ax.fill(data_to_plot, x[::configs[ele]["SIGN"]][mask], alpha=0.3, color=p0[0].get_color())
                ax.set_ylim(-70, 70)
                ax.set_xlim(-40, 40)
            else:
                data_to_plot = self._norm(data[ele][mask])
                x = np.linspace(0, len(data[ele]) * configs[ele]["PITCH"], len(data[ele])) + configs[ele]["OFFSET"]
                ax.add_patch(PolygonPatch(self.TCSC, facecolor='gray', edgecolor='red', zorder=2, alpha=0.2))
                p0 = ax.plot(x[::configs[ele]["SIGN"]][mask], data_to_plot, label=ele)
                ax.fill_between(x[::configs[ele]["SIGN"]][mask], 0, data_to_plot, alpha=0.3, color=p0[0].get_color())
                ax.set_ylim(-5, 30)
                ax.set_xlim(-10, 10)

        ax.set_xlabel("x / mm")
        ax.set_ylabel("y / mm")
        ax.legend(fontsize=8, ncol=2, mode="expand", frameon=True)
        self.ui.plotarea.canvas.draw()

    def get_material_upper(self, blade_thickness, d1, d2, theta1, theta2, height):
        if(blade_thickness <= 0):
            gap = -blade_thickness
            Ysept_shift = -np.tan(theta1)*gap/2 + gap
            jaws = Polygon([
                (gap/2, 0),
                (gap/2, np.tan(theta1)*gap/2 + Ysept_shift),
                (d1, np.tan(theta1)*d1 + Ysept_shift),
                (d2, np.tan(theta1)*d1 + np.tan(theta2)*(d2-d1) + Ysept_shift),
                (d2, height), (-d2, height),
                (-d2, np.tan(theta1)*d1 + np.tan(theta2)*(d2-d1) + Ysept_shift),
                (-d1, np.tan(theta1)*d1 + Ysept_shift),
                (-gap/2, np.tan(theta1)*gap/2 + Ysept_shift),
                (-gap/2, 0)])     
        else:
            R = -blade_thickness/(1 - 1/np.cos(theta1))
            theta = np.linspace(-np.pi/2, -np.pi/2 + theta1, 100)

            X = R*np.cos(theta)
            Y = R*np.sin(theta) + blade_thickness + R
            X = np.append(X, [d1, d2, d2, -d2, -d2, -d1])
            Y = np.append(Y, [np.tan(theta1)*d1,
                              np.tan(theta1)*d1 + np.tan(theta2)*(d2-d1),
                              height, height,
                              np.tan(theta1)*d1 + np.tan(theta2)*(d2-d1),
                              np.tan(theta1)*d1])
            theta = np.linspace(-np.pi/2 - theta1, -np.pi/2, 100)
            X = np.append(X, R*np.cos(theta))
            Y = np.append(Y, R*np.sin(theta) + blade_thickness + R)
            jaws = Polygon([(x,y) for x, y in np.vstack([X,Y]).T])
        return jaws

    def get_material_lower(self, left_width, right_width, height):
	    jaws = Polygon([(0, 0), (-left_width, 0), (-left_width, -height), (right_width, -height), (right_width, 0)])
	    return jaws

	
        
    def stopSubscription(self):
        self.japc.stopSubscriptions()
        self.japc.clearSubscriptions()
        
    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Message', "Are you sure to quit?",
                                     QMessageBox.Yes | QMessageBox.No,
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.stopSubscription()
            event.accept()
        else:
            event.ignore()
